package com.example.readbooks;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.example.readbooks.object.Libro;

import java.util.List;

public class BookAdapter extends RecyclerView.Adapter<BookAdapter.BookViewHolder> {

    List<Libro> libros;

    public BookAdapter(List<Libro> libros) {
        this.libros = libros;
    }

     class BookViewHolder extends RecyclerView.ViewHolder{

        TextView nombreLibro;
        TextView author;
        TextView estado;
        TextView puntuacion;
        TextView año;


        public BookViewHolder(@NonNull View itemView) {
            super(itemView);

            nombreLibro = itemView.findViewById(R.id.textViewTitle);
            author = itemView.findViewById(R.id.textViewAuthor);
            estado = itemView.findViewById(R.id.textViewEstado);
            puntuacion = itemView.findViewById(R.id.textViewPuntuación);
            año = itemView.findViewById(R.id.textViewAnyo);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //antes de crear esto hay que haber hecho las dependencias y los apply y hacer un rebuild, para comprobar si esta
                    //bien ir a la carpeta java/generated y mirar si nmo sha creado el directions.
                    //y tienes que poner el nav que poasas el libro
                    NavDirections navDirections = MenuPrincipalFragmentDirections.actionMenuPrincipalFragmentToModificarLibroFragment4(libros.get(getAdapterPosition()),getAdapterPosition());

                    Navigation.findNavController(v).navigate(navDirections);
                }
            });

        }
        public void bindData(Libro libro){
            nombreLibro.setText(libro.getTitle());
            author.setText("De: ".concat(libro.getAuthor()));
            estado.setText((libro.getStatus()));
            if (libro.getStatus().equals("Leído")){
                puntuacion.setText("Puntuación: "+libro.getPuntuation());
            }else {
                puntuacion.setText("");
            }

            año.setText("Lanzado  en: "+libro.getAnyo());
        }
    }


    @NonNull
    @Override
    public BookViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {

        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.book_item,parent,false);
        return new BookViewHolder(v);

    }

    @Override
    public void onBindViewHolder(@NonNull BookViewHolder holder, int position) {
        holder.bindData(libros.get(position));
    }

    @Override
    public int getItemCount() {
        return libros.size();
    }



}
