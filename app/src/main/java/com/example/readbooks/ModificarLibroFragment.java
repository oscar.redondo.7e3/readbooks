package com.example.readbooks;

import android.annotation.SuppressLint;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;

import com.example.readbooks.object.Libro;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.example.readbooks.BooksViewModel.libros;

public class ModificarLibroFragment extends Fragment {

    EditText editTextTitulo;
    EditText editTextAuthor;
    EditText editTextAnyo;
    Spinner spinnerEstado;
    RatingBar ratingBar;
    FloatingActionButton buttonAdd;
    Libro libro;
    Libro newLibro;
    TextView textVIewEstrellas;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @SuppressLint("ResourceAsColor")
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.modificar_libro_fragment,container,false);
        spinnerEstado = v.findViewById(R.id.spinner2);
        editTextTitulo = v.findViewById(R.id.editTextTitulo);
        editTextAuthor = v.findViewById(R.id.editTextAuthor);
        editTextAnyo = v.findViewById(R.id.editTextYear);
        ratingBar = v.findViewById(R.id.ratingBar);
        Drawable drawable = ratingBar.getProgressDrawable();
        buttonAdd = v.findViewById(R.id.buttonAñadir);
        textVIewEstrellas =v.findViewById(R.id.textViexPuntuacion);
        textVIewEstrellas.setVisibility(View.INVISIBLE);
        ratingBar.setVisibility(View.INVISIBLE);




        if (getArguments() !=null){
            libro = getArguments().getParcelable("libro");
            final int position = getArguments().getInt("pos");
            if (!libro.getTitle().equals("null")) {


                editTextTitulo.setText(libro.getTitle());
                editTextAuthor.setText(libro.getAuthor());
                spinnerEstado.setSelection(libro.getSpinnerPos());
                ratingBar.setRating((float) libro.getPuntuation());
                editTextAnyo.setText(String.valueOf(libro.getAnyo()));

        }
            if (spinnerEstado.getSelectedItemPosition()==2){
                textVIewEstrellas.setVisibility(View.VISIBLE);
                ratingBar.setVisibility(View.VISIBLE);
            }

            spinnerEstado.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    String selection = (String) parent.getItemAtPosition(position);

                    if ((selection).equals("Leído")){
                        textVIewEstrellas.setVisibility(View.VISIBLE);
                        ratingBar.setVisibility(View.VISIBLE);
                    }else {
                        textVIewEstrellas.setVisibility(View.INVISIBLE);
                        ratingBar.setVisibility(View.INVISIBLE);
                    }

                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }

            });

            buttonAdd.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    boolean  datos = comprobarDatos();
                    if (datos){
                        recogerDatos();
                        if (position==-1){
                            libros.add(newLibro);
                        }else {
                            libros.set(position,newLibro);
                        }

                        NavDirections navDirections = ModificarLibroFragmentDirections.actionModificarLibroFragmentToMenuPrincipalFragment1();

                        Navigation.findNavController(v).navigate(navDirections);
                    }else {
                        Toast.makeText(getActivity(),"Error debes rellenar todos los campos",Toast.LENGTH_SHORT).show();
                    }


                }
            });
        }



        return v;


    }

    public void recogerDatos(){

        double stars ;
        if (ratingBar.getVisibility()==View.INVISIBLE){
            stars=0.0;
        }else {
            stars = ratingBar.getRating();
        }

        newLibro= new Libro((editTextTitulo.getText().toString()),
        (editTextAuthor.getText().toString()),
        (spinnerEstado.getSelectedItem().toString()),
         stars,
                Integer.parseInt(editTextAnyo.getText().toString()));




    }

    public boolean  comprobarDatos(){

        if (editTextTitulo.getText().toString().equals("")){
            return false;
        }else if (editTextAuthor.getText().toString().equals("")){
            return false;
        }else if (editTextAnyo.getText().toString().equals("")){
            return false;
        }else return true;


    }
}
