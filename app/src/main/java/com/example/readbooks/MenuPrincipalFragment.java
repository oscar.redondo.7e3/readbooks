package com.example.readbooks;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProvider;
import androidx.navigation.NavDirections;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.readbooks.object.Libro;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import static com.example.readbooks.BooksViewModel.libros;

public class MenuPrincipalFragment extends Fragment {


    RecyclerView listaLibros;
    BooksViewModel booksViewModel;
    FloatingActionButton  badd;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //esta es la linea para que coja los libros que ya tienes creados de antes
        booksViewModel = new ViewModelProvider(getActivity()).get(BooksViewModel.class);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.menu_principal_fragment,container,false);
        listaLibros = v.findViewById(R.id.recyclerView);
        //si quieres linear o grid se decide en esta linea y se tiene que poner el numero de columnas
        listaLibros.setLayoutManager(new GridLayoutManager(getContext(),2));
        BookAdapter adapter = new BookAdapter(libros);
        badd  = v.findViewById(R.id.floatting);
        listaLibros.setAdapter(adapter);

        badd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Libro l  =new Libro("null","w","Pendiente",20.0,2000);

               NavDirections navDirections = MenuPrincipalFragmentDirections.actionMenuPrincipalFragmentToModificarLibroFragment(l,-1);

                Navigation.findNavController(v).navigate(navDirections);
            }
        });

        return v;

    }



}
