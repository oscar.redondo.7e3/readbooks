package com.example.readbooks.object;

import android.os.Parcel;
import android.os.Parcelable;

public class Libro implements Parcelable {

    private String title;
    private String author;
    private String status;
    private Double puntuation;
    private int anyo;


    public Libro(String title, String author, String status, Double puntuation, int anyo) {
        this.title = title;
        this.author = author;
        this.status = status;
        this.puntuation = puntuation;
        this.anyo = anyo;
    }

    protected Libro(Parcel in) {
        title = in.readString();
        author = in.readString();
        status = in.readString();
        if (in.readByte() == 0) {
            puntuation = null;
        } else {
            puntuation = in.readDouble();
        }
        anyo = in.readInt();
    }

    public static final Creator<Libro> CREATOR = new Creator<Libro>() {
        @Override
        public Libro createFromParcel(Parcel in) {
            return new Libro(in);
        }

        @Override
        public Libro[] newArray(int size) {
            return new Libro[size];
        }
    };

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public double getPuntuation() {
        return puntuation;
    }

    public void setPuntuation(double puntuation) {
        this.puntuation = puntuation;
    }

    public void setPuntuation(Double puntuation) {
        this.puntuation = puntuation;
    }

    public int getAnyo() {
        return anyo;
    }

    public void setAnyo(int anyo) {
        this.anyo = anyo;
    }

    public int getSpinnerPos(){
        switch (status){

            case "Pendiente":
                return  0;
            case "Leyendo":
                return 1;
            case "Leído":
                return 2;
            default:return 0;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(author);
        dest.writeString(status);
        dest.writeDouble(puntuation);
        dest.writeInt(anyo);
    }
}
