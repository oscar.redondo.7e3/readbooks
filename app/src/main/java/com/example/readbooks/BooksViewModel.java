package com.example.readbooks;

import com.example.readbooks.object.Libro;

import java.util.ArrayList;
import java.util.List;

public class BooksViewModel extends androidx.lifecycle.ViewModel{

    static ArrayList<Libro> libros = new ArrayList<Libro>();
    Libro l1 = new Libro("La cinquena onada","Rick Yancey","Pendiente",5.0,2013);
    Libro l2 = new Libro("Harry Potter y el prisionero de Azkaban", "J. K. Rowling","Leyendo",4.0,1999);
    Libro l3 = new Libro("Rimas y leyendas", "Gustavo Adolfo Bécquer","Pendiente",2.0,1871);
    Libro l4 = new Libro("Harry Potter y el prisionero de Azkaban", "J. K. Rowling","Pendiente",4.0,1999);
    Libro l5 = new Libro("Harry Potter y el prisionero de Azkaban", "J. K. Rowling","Leyendo",4.0,1999);
    Libro l6 = new Libro("Harry Potter y el prisionero de Azkaban", "J. K. Rowling","Leído",4.6,1999);


    public BooksViewModel() {
        libros.add(l1);
        libros.add(l2);
        libros.add(l3);
        libros.add(l4);
        libros.add(l5);
        libros.add(l6);
    }
}
